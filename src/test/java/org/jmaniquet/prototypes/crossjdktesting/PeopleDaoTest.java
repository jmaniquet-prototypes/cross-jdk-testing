package org.jmaniquet.prototypes.crossjdktesting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.jmaniquet.prototypes.crossjdktesting.JdbcUtils.toTimestamp;

import java.time.LocalDateTime;

import javax.sql.DataSource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

@SpringBootTest
public class PeopleDaoTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	private static final Logger logger = LoggerFactory.getLogger(PeopleDaoTest.class);
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private PeopleDao underTest;

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@BeforeEach
	public void setup() {
		logger.info("DataSource type : {}", dataSource.getClass());
	}
	
	@Test
	public void testInsert() {
		String name = "some-name";
		LocalDateTime now = LocalDateTime.now();
		
		People p = new People();
		p.setName(name);
		p.setBirthDate(now);
		
		underTest.insert(p);
		
		Integer count = jdbcTemplate.queryForObject("select count(*) from people where name = ? and birth_date = ?", int.class, name, toTimestamp(now));
		assertThat(count).isEqualTo(1);
	}
	
	@Test
	@Sql("people-test.sql")
	public void testUpdate() {
		LocalDateTime minusYears = LocalDateTime.now().minusYears(1);
		String name = "some-name-2";
		
		People p = new People();
		p.setPeopleId(1l);
		p.setName(name);
		p.setBirthDate(minusYears);
		
		underTest.update(p);
		
		Integer count = jdbcTemplate.queryForObject("select count(*) from people where name = ? and birth_date = ?", int.class, name, toTimestamp(minusYears));
		assertThat(count).isEqualTo(1);
	}
	
	@Test
	@Sql("people-test.sql")
	public void testSelect() {
		LocalDateTime now = LocalDateTime.now().minusSeconds(30);
		
		People result = underTest.select(1l);
		
		assertThat(result).isNotNull();
		assertSoftly(softly -> {
			softly.assertThat(result.getPeopleId()).isEqualTo(1);
			softly.assertThat(result.getName()).isEqualTo("some-name");
			softly.assertThat(result.getBirthDate()).isAfter(now); // FIXME : use transactional current_timestamp
		});
	}
	
	@Test
	@Sql("people-test.sql")
	public void testDelete() {
		underTest.delete(1l);
		
		Integer count = jdbcTemplate.queryForObject("select count(*) from people", int.class);
		assertThat(count).isEqualTo(0);
	}
}
