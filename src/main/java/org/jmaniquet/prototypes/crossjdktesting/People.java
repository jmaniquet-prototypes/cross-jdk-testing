package org.jmaniquet.prototypes.crossjdktesting;

import java.time.LocalDateTime;

public class People {

	private Long peopleId;
	
	private String name;
	
	private LocalDateTime birthDate;

	public Long getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Long userId) {
		this.peopleId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDateTime birthDate) {
		this.birthDate = birthDate;
	}

}
