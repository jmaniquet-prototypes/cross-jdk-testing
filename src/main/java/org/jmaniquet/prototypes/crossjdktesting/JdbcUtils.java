package org.jmaniquet.prototypes.crossjdktesting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class JdbcUtils {

	private JdbcUtils() {}

	public static LocalDateTime getLocalDateTime(ResultSet rs, String colName) throws SQLException {
		Timestamp jdbcBirthDate = rs.getTimestamp(colName);
		LocalDateTime birthDate = jdbcBirthDate != null ? jdbcBirthDate.toLocalDateTime() : null;
		return birthDate;
	}
	
	public static Long nullableLong(ResultSet rs, String colName) throws SQLException {
		long jdbcId = rs.getLong(colName);
		Long id = !rs.wasNull() ? jdbcId : null;
		return id;
	}
	
	public static Timestamp toTimestamp(LocalDateTime ldt) {
		Timestamp ts = null;
		if (ldt != null) {
			ts = Timestamp.valueOf(ldt);
		}
		return ts;
	}
}
