package org.jmaniquet.prototypes.crossjdktesting;

import static org.jmaniquet.prototypes.crossjdktesting.JdbcUtils.toTimestamp;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class PeopleDaoImpl implements PeopleDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private RowMapper<People> rowMapper;
	
	@Override
	public void insert(People user) {
		LocalDateTime birthDate = user.getBirthDate();
		jdbcTemplate.update("insert into people (name, birth_date) values (?, ?)", user.getName(), toTimestamp(birthDate));
	}

	@Override
	public void update(People user) {
		LocalDateTime birthDate = user.getBirthDate();
		jdbcTemplate.update("update people set name = ?, birth_date = ? where people_id = ?", user.getName(), toTimestamp(birthDate), user.getPeopleId());
	}

	@Override
	public People select(Long userId) {
		return jdbcTemplate.queryForObject("select people_id, name, birth_date from people where people_id = ?", rowMapper, userId);
	}

	@Override
	public void delete(Long userId) {
		jdbcTemplate.update("delete from people where people_id = ?", userId);
	}
}
