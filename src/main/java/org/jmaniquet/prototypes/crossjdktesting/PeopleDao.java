package org.jmaniquet.prototypes.crossjdktesting;

public interface PeopleDao {

	void insert(People user);
	
	void update(People user);
	
	People select(Long userId);
	
	void delete(Long userId);
}
